from flask_wtf import FlaskForm
from wtforms import TextAreaField, SubmitField, IntegerField

class AddRequestForm(FlaskForm):
	user_id = IntegerField("Your User ID")
	description = TextAreaField("Description of Request")
	submit = SubmitField("Create")
