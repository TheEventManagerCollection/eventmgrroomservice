from app import app
from flask import render_template, flash, url_for, redirect

from forms import *

from EventMgrAPI.EventInformation import EventInformation
from EventMgrAPI.User import User

event_info = EventInformation()

@app.context_processor
def inject_user():
    return dict(event_info=event_info)


@app.route('/test')
def test():
	return "Test"

@app.route('/index')
@app.route('/')
def home():
	return render_template('home.html', title='Home')

@app.route('/add_rs_request', methods=["GET", "POST"])
def add_rs_request():
	form = AddRequestForm()
	
	if form.validate_on_submit():
		user = User(form.user_id.data)
		description = form.description.data
		
		user.create_rs_request(description)
		flash("Room service request added succesfully!")
		return redirect(url_for("home"))
	
	return render_template('request.html', title="Home", form=form)

